# H1 Clean-me-up
The clean-me-up project is rather ugly and the task is to clean it up to make the code more maintainable and follow good coding practice.

The application exposes a REST end-point for sending email. 
The contract has not yet been published so you don't need to be backwards compatible.

When you are done you should be rather happy with the code when it comes to quality, structure, test, validation etc.
If you left things behind - please note that down in a read me file.

Estimated time ~2 hours. 

You should only do changes in the **clean-me-up-rest** module, don't change the **clean-me-up-support** module.

# Getting started
In order to run the service the `clean-me-up-support` package must've been built. 

The service can be built and started with the following command(s) from root:

```
# only needed once to install the support package
./mvnw install 
./mvnw -f clean-me-up-rest spring-boot:run
```

The following example curl request can be used to test the service
```
curl -v -XPOST -H "Content-type: application/json" -d '{"addresses":["dc3dfee2-68e7-4ee3-83ad-8c4309a2db7f@email.com","405a002f-5ad4-4d26-88cf-baa6d7b9b138@email.com"],"subject":"b09d1b5b-e652-46bc-bfdc-f638237c8cf3","content":"0845686d-9738-40e2-bc8d-416fa91027c8"}' 'http://localhost:8080/api/v1/emails'
```

# Docker
In order to create a docker image run the following command from root

```
./mvnw -f clean-me-up-rest install dockerfile:build
```

Running the image can be achieved by: 
```
docker run -d -p 8080:8080 clean-me-up/clean-me-up-rest:latest
```

# Worklog

# Functionality change
1. In previous implementation, empty string as subject was considered valid, in my implementation it is not. 
2. Bumped the Java version of the clean-me-up-support package since it didn't build with the Java version I was working with.

# Things left out

## Improvements
1. If more beans would be required, I would move it from the Application class
2. return `void` instead of ResponseEntity<Void> from EmailController

## Features
1. Optional fields for CC and BCC
2. Recipient variables in order to replace values dynamically for batch sending.