package com.effcode.clean.me.rest;

/**
 * Constant class containing all field names of rest requests and responses
 *
 * @author palmithor
 * @since 2018-08-30
 */
public class FieldConstant {

    public static final String ADDRESSES = "addresses";
    public static final String SUBJECT = "subject";
    public static final String CONTENT = "content";

    private FieldConstant() {

    }
}
