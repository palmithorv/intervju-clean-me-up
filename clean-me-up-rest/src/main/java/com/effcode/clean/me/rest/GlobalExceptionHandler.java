package com.effcode.clean.me.rest;

import com.effcode.clean.me.rest.common.MessageByLocaleService;
import com.effcode.clean.me.rest.model.ErrorCode;
import com.effcode.clean.me.rest.model.ErrorResponse;
import com.effcode.clean.me.rest.model.FieldConstraintViolation;
import com.effcode.clean.me.rest.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

/**
 * @author palmithor
 * @since 2018-08-30
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    private MessageByLocaleService messageByLocaleService;

    public GlobalExceptionHandler(final MessageByLocaleService messageByLocaleService) {
        this.messageByLocaleService = messageByLocaleService;
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleUnexpectedException(final Exception e) {
        logger.error(e.getMessage(), e);
        ErrorCode errorCode = ErrorCode.INTERNAL_SERVER_ERROR;
        return errorResponseToResponseEntity(ErrorResponse
                .serverError()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()))
                .build());
    }


    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleConstraintViolationException(final ConstraintViolationException e) {
        logger.trace(e.getMessage(), e);
        final ErrorCode errorCode = ErrorCode.VALIDATION_FAILED;
        return errorResponseToResponseEntity(ErrorResponse.badRequest()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()))
                .addFieldViolation(e)
                .build());
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorResponse> handleMethodArgumentTypeMismatchException(final MethodArgumentTypeMismatchException e) {
        logger.trace(e.getMessage(), e);
        final ErrorCode errorCode = ErrorCode.VALIDATION_FAILED;
        return errorResponseToResponseEntity(ErrorResponse.badRequest()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()))
                .addFieldViolation(e.getName(), e.getValue(), messageByLocaleService.getMessage(ErrorCode.INVALID_TYPE.messageKey()))
                .build());
    }

    @ExceptionHandler(value = HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleHttpMediaTypeNotSupportedException(final HttpMediaTypeNotSupportedException e) {
        logger.trace(e.getMessage(), e);
        final ErrorCode errorCode = ErrorCode.UNSUPPORTED_MEDIA_TYPE;
        return errorResponseToResponseEntity(ErrorResponse.badRequest()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()))
                .build());
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ErrorResponse> handleHttpRequestMethodNotSupportedException(final HttpRequestMethodNotSupportedException e) {
        logger.trace(e.getMessage(), e);
        final ErrorCode errorCode = ErrorCode.METHOD_NOT_ALLOWED;
        return errorResponseToResponseEntity(ErrorResponse.methodNotAllowed()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()))
                .build());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleInvalidRequest(final MethodArgumentNotValidException e) {
        logger.trace(e.getMessage(), e);
        final ErrorCode errorCode = ErrorCode.VALIDATION_FAILED;
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        final ErrorResponse.Builder builder = ErrorResponse.badRequest()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()));
        if (e.getBindingResult().getFieldErrorCount() > 0 || e.getBindingResult().getGlobalErrorCount() == 0) {
            StreamUtils.asStream(fieldErrors.iterator())
                    .forEach(fieldError -> builder.addFieldViolation(fieldError.getField(),
                            fieldError.getRejectedValue(),
                            fieldError.getDefaultMessage()));

            return errorResponseToResponseEntity(builder.build());
        } else {
            final String detail = Optional.ofNullable(e.getBindingResult().getGlobalError())
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .orElse(null);
            return errorResponseToResponseEntity(builder.addDetail(detail).build());
        }
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> handleRequestBodyMissing(final HttpMessageNotReadableException e) {
        logger.trace(e.getMessage(), e);
        final ErrorCode errorCode = ErrorCode.MALFORMED_REQUEST;
        return errorResponseToResponseEntity(ErrorResponse.badRequest()
                .code(errorCode.value())
                .message(messageByLocaleService.getMessage(errorCode.messageKey()))
                .build());
    }


    private ResponseEntity<ErrorResponse> errorResponseToResponseEntity(final ErrorResponse errorResponse) {
        return ResponseEntity
                .status(errorResponse.getStatusCode())
                .body(errorResponse);
    }
}
