package com.effcode.clean.me.rest.email;

import com.effcode.clean.me.rest.FieldConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.validation.constraints.Email;

/**
 * @author palmithor
 * @since 2018-08-30
 */
public class EmailAddress {

    private final String address;

    public EmailAddress() {
        this.address = null;
    }

    public EmailAddress(final String address) {
        this.address = address;
    }

    @JsonValue
    @Email
    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return address;
    }
}
