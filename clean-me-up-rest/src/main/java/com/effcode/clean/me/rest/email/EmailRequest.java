package com.effcode.clean.me.rest.email;

import com.effcode.clean.me.rest.FieldConstant;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Email request POJO.
 *
 * Consists of all fields required for sending
 *
 * @author palmithor
 * @since 2018-08-30
 */
public class EmailRequest {

    private final List<EmailAddress> addresses;
    private final String subject;
    private final String content;

    // Empty constructor required for Jackson for immutable objects
    public EmailRequest() {
        this.addresses = null;
        this.subject = null;
        this.content = null;
    }

    public EmailRequest(final List<EmailAddress> addresses, final String subject, final String content) {
        this.addresses = addresses;
        this.subject = subject;
        this.content = content;
    }

    @JsonProperty(FieldConstant.ADDRESSES)
    @Valid
    @NotEmpty
    public List<EmailAddress> getAddresses() {
        return addresses;
    }

    @JsonProperty(FieldConstant.SUBJECT)
    @NotBlank
    public String getSubject() {
        return subject;
    }

    @JsonProperty(FieldConstant.CONTENT)
    @Length(max = 65000)
    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "addresses=" + addresses +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'';
    }


}
