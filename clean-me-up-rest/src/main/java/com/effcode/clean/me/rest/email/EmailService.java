package com.effcode.clean.me.rest.email;

import com.effcode.clean.me.support.SmtpEmail;
import com.effcode.clean.me.support.SmtpHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.ws.ServiceMode;

@Service
public class EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    private final SmtpHandler smtpHandler;
    private final String username;
    private final String password;

    public EmailService(final SmtpHandler smtpHandler,
                        @Value("${app.smtp.username:foo}") final String username,
                        @Value("${app.smtp.password:password}") final String password) {
        this.smtpHandler = smtpHandler;
        this.username = username;
        this.password = password;
    }


    /**
     * Send email
     * <p>
     * The function constructs the {@link SmtpEmail} object and posts via the {@link SmtpHandler}.
     * Note that the method requires the request to have already been validated.
     *
     * @param request containing all required data
     */
    void send(final EmailRequest request) {
        logger.debug("Email request received: {} ", request);
        SmtpEmail smtpEmail = new SmtpEmail();
        smtpEmail.adrs = request.getAddresses().stream().map(EmailAddress::getAddress).toArray(String[]::new);
        smtpEmail.content = request.getContent();
        smtpEmail.subject = request.getSubject();
        smtpEmail.username = username;
        smtpEmail.password = password;
        smtpHandler.post(smtpEmail);
        logger.info("Email with subject has been sent to {}", request.getAddresses());
    }

}
