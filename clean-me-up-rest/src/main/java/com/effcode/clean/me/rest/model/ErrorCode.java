package com.effcode.clean.me.rest.model;

/**
 * @author palmithor
 * @since 2017-09-07
 */
public enum ErrorCode {

    // 400*
    VALIDATION_FAILED(40001, "error.validation_failed"),
    MALFORMED_REQUEST(40002, "error.malformed_request"),
    INVALID_TYPE(40003, "error.invalid_type"),
    MISSING_REQUIRED_QUERY_PARAM(40004, "error.missing_required_query_param"),
    INVALID_SORT_PARAM(40005, "error.unsupported_sort_param"),
    UNSUPPORTED_MEDIA_TYPE(40006, "error.unsupported_media_type"),

    // 401*
    UNAUTHORIZED(40101, "error.unauthorized"),

    // 403*
    FORBIDDEN(40300, "error.forbidden"),

    // 404*
    URL_NOT_FOUND(40400, "error.resource_not_found"),

    // 405*
    METHOD_NOT_ALLOWED(40500, "error.method_not_allowed"),

    INTERNAL_SERVER_ERROR(50000, "error.internal_server_error"),
    ;

    private Integer value;
    private String messageKey;

    ErrorCode(final Integer value, final String messageKey) {
        this.value = value;
        this.messageKey = messageKey;
    }

    public Integer value() {
        return value;
    }

    public String messageKey() {
        return messageKey;
    }
}
