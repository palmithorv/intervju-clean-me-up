package com.effcode.clean.me.rest.model;

import com.effcode.clean.me.rest.util.ConstraintViolationUtil;
import com.effcode.clean.me.rest.util.StreamUtils;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ErrorResponse implements Serializable {

    private final Integer statusCode;
    private final String status;
    private final Integer code;
    private final String message;
    private final List<String> details;
    private final List<FieldConstraintViolation> fieldViolations;

    public ErrorResponse() {
        this.statusCode = null;
        this.status = null;
        this.code = null;
        this.message = null;
        this.details = null;
        this.fieldViolations = null;
    }

    public ErrorResponse(final Integer statusCode, final String status, final Integer code, final String message, final List<String> details, final List<FieldConstraintViolation> fieldViolations) {
        this.statusCode = statusCode;
        this.status = status;
        this.code = code;
        this.message = message;
        this.details = details;
        this.fieldViolations = fieldViolations;
    }

    /**
     * The HTTP status code mapping
     */
    @JsonProperty("statusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * This corresponds to HTTP status code message
     */
    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    /**
     * A simple error code that can be easily handled by a client.
     */
    @JsonProperty("code")
    public Integer getCode() {
        return code;
    }

    /**
     * A developer-facing human-readable error message in English. It should
     * both explain the error and offer an actionable resolution to it.
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * Additional error information that the client code can use to handle
     * the error, such as retry delay or a help link.
     */
    @JsonProperty("details")
    public List<String> getDetails() {
        return details;
    }

    /**
     * A list containing all of the constraint violation the request
     */
    @JsonProperty("fieldViolations")
    public List<FieldConstraintViolation> getFieldViolations() {
        return fieldViolations;
    }

    public static Builder create() {
        return new Builder();
    }


    /**
     * Sets the status code to 400 and the status to "Bad Request"
     */
    public static Builder badRequest() {
        Builder builder = new Builder();
        builder.statusCode = 400;
        builder.status = "Bad Request";
        return builder;
    }

    /**
     * Sets the status code to 401 and the status to "Unauthorized"
     */
    public static Builder unauthorized() {
        Builder builder = new Builder();
        builder.statusCode = 401;
        builder.status = "Unauthorized";
        return builder;
    }

    /**
     * Sets the status code to 403 and the status to "Forbidden"
     */
    public static Builder forbidden() {
        Builder builder = new Builder();
        builder.statusCode = 403;
        builder.status = "Forbidden";
        return builder;
    }

    /**
     * Sets the status code to 404 and the status to "Not Found"
     */
    public static Builder notFound() {
        Builder builder = new Builder();
        builder.statusCode = 404;
        builder.status = "Not Found";
        return builder;
    }

    /**
     * Sets the status code to 405 and the status to "Method Not Allowed"
     */
    public static Builder methodNotAllowed() {
        Builder builder = new Builder();
        builder.statusCode = 405;
        builder.status = "Method Not Allowed";
        return builder;
    }

    /**
     * Sets the status code to 409 and the status to "Conflict"
     */
    public static Builder conflict() {
        Builder builder = new Builder();
        builder.statusCode = 409;
        builder.status = "Conflict";
        return builder;
    }

    /**
     * Sets the status code to 429 and the status to "Too Many Requests"
     */
    public static Builder tooManyRequests() {
        Builder builder = new Builder();
        builder.statusCode = 429;
        builder.status = "Too Many Requests";
        return builder;
    }

    /**
     * Sets the status code to 500 and the status to "Internal Server Error"
     */
    public static Builder serverError() {
        Builder builder = new Builder();
        builder.statusCode = 500;
        builder.status = "Internal Server Error";
        return builder;
    }

    /**
     * Sets the status code to 504 and the status to "Gateway Timeout"
     */
    public static Builder gatewayTimeout() {
        Builder builder = new Builder();
        builder.statusCode = 504;
        builder.status = "Gateway Timeout";
        return builder;
    }

    /**
     * Sets the status code to 499 and the status to "Client Closed Request"
     * This status is influenced by code 499 which was introduced by nginx
     *
     * @see <a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes">Wikipedia Http Status Codes</a>
     */
    public static Builder clientClosedRequest() {
        Builder builder = new Builder();
        builder.statusCode = 499;
        builder.status = "Client Closed Request";
        return builder;
    }


    public static Builder copy(final ErrorResponse errorResponse) {
        return create()
                .statusCode(errorResponse.getStatusCode())
                .status(errorResponse.getStatus())
                .code(errorResponse.getCode())
                .message(errorResponse.getMessage())
                .details(errorResponse.getDetails())
                .fieldViolations(errorResponse.getFieldViolations());
    }

    public static final class Builder {
        private Integer statusCode;
        private String status;
        private Integer code;
        private String message;
        private List<String> details;

        private List<FieldConstraintViolation> fieldViolations;

        private Builder() {
        }

        /**
         * Set HTTP status code
         */
        public Builder statusCode(final Integer statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        /**
         * Set HTTP status message
         */
        public Builder status(final String status) {
            this.status = status;
            return this;
        }

        /**
         * Set custom code
         */
        public Builder code(final Integer code) {
            this.code = code;
            return this;
        }

        /**
         * Set custom message
         */
        public Builder message(final String message) {
            this.message = message;
            return this;
        }

        /**
         * Set error details
         */
        public Builder details(final List<String> details) {
            this.details = details;
            return this;
        }

        public Builder addDetail(final String detailedMessage) {
            if (this.details == null) {
                this.details = new ArrayList<>();
            }
            this.details.add(detailedMessage);
            return this;
        }

        /**
         * Field violations
         */
        public Builder fieldViolations(final List<FieldConstraintViolation> fieldViolations) {
            this.fieldViolations = fieldViolations;
            return this;
        }

        /**
         * Set field violations
         */
        public Builder addFieldViolation(final ConstraintViolationException constraintViolationException) {
            if (this.fieldViolations == null) {
                this.fieldViolations = new ArrayList<>();
            }
            this.fieldViolations.addAll(StreamUtils.asStream(constraintViolationException.getConstraintViolations().iterator())
                    .map(ConstraintViolationUtil::convertFromFieldConstraintViolation)
                    .collect(Collectors.toList()));
            return this;
        }

        /**
         * Set field violations
         */
        public Builder addFieldViolation(final String field, final Object rejectedValue, final String message) {
            if (this.fieldViolations == null) {
                this.fieldViolations = new ArrayList<>();
            }
            this.fieldViolations.add(new FieldConstraintViolation(field, rejectedValue, message));
            return this;
        }

        public ErrorResponse build() {
            return new ErrorResponse(statusCode, status, code, message, details, fieldViolations);
        }
    }
}
