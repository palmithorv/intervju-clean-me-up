package com.effcode.clean.me.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author palmithor
 * @since 2017-09-04
 */
public class FieldConstraintViolation implements Serializable {

    private final String field;
    private final Object rejectedValue;
    private final String message;

    public FieldConstraintViolation() {
        this.field = null;
        this.rejectedValue = null;
        this.message = null;
    }

    public FieldConstraintViolation(final String field, final Object rejectedValue, final String message) {
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.message = message;
    }

    @JsonProperty("field")
    public String getField() {
        return field;
    }


    @JsonProperty("rejectedValue")
    public Object getRejectedValue() {
        return rejectedValue;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    public static Builder createBuilder() {
        return Builder.create();
    }

    public static final class Builder {
        private String field;
        private Object rejectedValue;
        private String message;

        private Builder() {
        }

        public static Builder create() {
            return new Builder();
        }

        public Builder field(String field) {
            this.field = field;
            return this;
        }

        @Deprecated
        public Builder title(String field) {
            this.field = field;
            return this;
        }

        public Builder rejectedValue(Object rejectedValue) {
            this.rejectedValue = rejectedValue;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public FieldConstraintViolation build() {
            return new FieldConstraintViolation(field, rejectedValue, message);
        }
    }
}
