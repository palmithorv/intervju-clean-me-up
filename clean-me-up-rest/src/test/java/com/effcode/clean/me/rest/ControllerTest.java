package com.effcode.clean.me.rest;


import com.effcode.clean.me.rest.common.MessageByLocaleService;
import com.effcode.clean.me.rest.configuration.JacksonConfiguration;
import com.effcode.clean.me.rest.configuration.LocaleConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.effcode.clean.me.rest.configuration.JacksonConfiguration.getObjectMapper;

/**
 * @author palmithor
 * @since 2017-09-14
 */
@RunWith(SpringRunner.class)
@ImportAutoConfiguration({LocaleConfiguration.class, JacksonConfiguration.class})
@Import({MessageByLocaleService.class})
public abstract class ControllerTest {

    @Autowired protected MockMvc mockMvc;
    protected final ObjectMapper objectMapper;

    public ControllerTest() {
        this.objectMapper = getObjectMapper();
    }
}
