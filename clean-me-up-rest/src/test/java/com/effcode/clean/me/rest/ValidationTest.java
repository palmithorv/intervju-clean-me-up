package com.effcode.clean.me.rest;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static com.effcode.clean.me.rest.util.StreamUtils.asStream;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author palmithor
 * @since 2017-07-20.
 */
@RunWith(SpringRunner.class)
@Import({ValidationAutoConfiguration.class})
public abstract class ValidationTest {

    protected Validator validator;

    @Autowired private LocalValidatorFactoryBean validatorFactoryBean;

    @Before
    public void setUp() {
        this.validator = validatorFactoryBean.getValidator();
    }

    protected <T> ConstraintViolation<T> validateAndGetSingleViolation(final T request) {
        Set<ConstraintViolation<T>> constraints = validator.validate(request);
        assertThat(constraints).hasSize(1);
        return constraints.iterator().next();
    }

    protected <T> void assertMustNotBeNull(final ConstraintViolation<T> constraintViolation, final String field) {
        assertThat(constraintViolation.getInvalidValue()).isNull();
        assertThat(constraintViolation.getMessage()).isEqualTo("may not be null");
        assertThat(asStream(constraintViolation.getPropertyPath().iterator())
                .reduce((first, second) -> second).get().toString()).isEqualTo(field);
    }

    protected <T> void assertListMustNotBeEmpty(final ConstraintViolation<T> constraintViolation, final String field, final Boolean isNull) {
        assertThat(constraintViolation.getInvalidValue()).isEqualTo(isNull ? null : emptyList());
        assertThat(constraintViolation.getMessage()).isEqualTo("must not be empty");
        assertThat(asStream(constraintViolation.getPropertyPath().iterator())
                .reduce((first, second) -> second).get().toString()).isEqualTo(field);
    }

    protected <T> void assertNotBlank(final ConstraintViolation<T> constraintViolation, final String field, final Boolean isNull) {
        assertThat(constraintViolation.getInvalidValue()).isEqualTo(isNull ? null : "");
        assertThat(constraintViolation.getMessage()).isEqualTo("must not be blank");
        assertThat(asStream(constraintViolation.getPropertyPath().iterator())
                .reduce((first, second) -> second).get().toString()).isEqualTo(field);
    }

    protected <T> void assertSize(final ConstraintViolation<T> constraintViolation, final String field, final Object value,
                                  final Integer min, final Integer max) {
        assertThat(constraintViolation.getInvalidValue()).isEqualTo(value);
        assertThat(constraintViolation.getMessage()).isEqualTo(String.format("length must be between %d and %d", min, max));
        assertThat(asStream(constraintViolation.getPropertyPath().iterator())
                .reduce((first, second) -> second).get().toString()).isEqualTo(field);
    }

}
