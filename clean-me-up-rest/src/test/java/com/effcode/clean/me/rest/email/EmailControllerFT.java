package com.effcode.clean.me.rest.email;

import com.effcode.clean.me.rest.ControllerFunctionalTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author palmithor
 * @since 2018-08-30
 */
public class EmailControllerFT extends ControllerFunctionalTest {

    private static final String BASE_PATH = "/api/v1/emails";

    @Test
    public void send() {
        final ResponseEntity<Void> responseEntity = restTemplate.postForEntity(BASE_PATH,
                new HttpEntity<>(EmailDataUtils.requestAllValid().build()),
                Void.class
        );
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
