package com.effcode.clean.me.rest.email;


import com.effcode.clean.me.rest.ControllerTest;
import com.effcode.clean.me.rest.model.ErrorResponse;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static com.effcode.clean.me.rest.configuration.JacksonConfiguration.getObjectMapper;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author palmithor
 * @since 2018-08-30
 */
@WebMvcTest(value = EmailController.class)
public class EmailControllerTest extends ControllerTest {

    private static final String BASE_PATH = "/api/v1/emails";
    @MockBean private EmailService emailService;

    @Test
    public void send() throws Exception {
        System.out.println(objectMapper.writeValueAsString(EmailDataUtils.requestAllValid().build()));
        doNothing().when(emailService).send(any());
        mockMvc.perform(post(BASE_PATH)
                        .content(objectMapper.writeValueAsString(EmailDataUtils.requestAllValid().build()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void failSendUnknownError() throws Exception {
        doThrow(new RuntimeException("Unexpected")).when(emailService).send(any());
        final MvcResult mvcResult = mockMvc.perform(post(BASE_PATH)
                .content(objectMapper.writeValueAsString(EmailDataUtils.requestAllValid().build()))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is5xxServerError())
                .andReturn();
        final ErrorResponse errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo("Internal Server Error");
        assertThat(errorResponse.getStatusCode()).isEqualTo(500);
        assertThat(errorResponse.getMessage()).isEqualTo("Internal server error");
        assertThat(errorResponse.getCode()).isEqualTo(50000);
    }

    @Test
    public void failSendMethodNotAllowed() throws Exception {
        mockMvc.perform(put(BASE_PATH)
                .content(objectMapper.writeValueAsString(EmailDataUtils.requestAllValid().build()))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void failSendInvalidBody() throws Exception {
        mockMvc.perform(post(BASE_PATH)
                .content("not-json")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void failSendValidationFailed() throws Exception {
        final MvcResult mvcResult = mockMvc.perform(post(BASE_PATH)
                .content(objectMapper.writeValueAsString(EmailDataUtils.requestAllValid().subject(null).build()))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andReturn();
        final ErrorResponse errorResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(errorResponse.getStatus()).isEqualTo("Bad Request");
        assertThat(errorResponse.getStatusCode()).isEqualTo(400);
        assertThat(errorResponse.getMessage()).isEqualTo("Validation failed");
        assertThat(errorResponse.getCode()).isEqualTo(40001);
    }
}