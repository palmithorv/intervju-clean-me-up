package com.effcode.clean.me.rest.email;

import java.util.UUID;

/**
 * Helper class used by tests that require test data
 *
 * @author palmithor
 * @since 2018-08-30
 */
public class EmailDataUtils {

    private EmailDataUtils() {

    }

    public static EmailRequestBuilder requestAllValid() {
        return EmailRequestBuilder.create()
                .subject(UUID.randomUUID().toString())
                .content(UUID.randomUUID().toString())
                .addAddress(UUID.randomUUID().toString() + "@email.com")
                .addAddress(UUID.randomUUID().toString() + "@email.com");
    }
}
