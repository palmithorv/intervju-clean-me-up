package com.effcode.clean.me.rest.email;

import java.util.ArrayList;
import java.util.List;

/**
 * @author palmithor
 * @since 2018-08-30
 */
public final class EmailRequestBuilder {

    private List<EmailAddress> addresses;
    private String subject;
    private String content;

    private EmailRequestBuilder() {
    }

    public static EmailRequestBuilder create() {
        return new EmailRequestBuilder();
    }

    public static EmailRequestBuilder copy(final EmailRequest request) {
        return new EmailRequestBuilder()
                .addresses(request.getAddresses())
                .subject(request.getSubject())
                .content(request.getContent());
    }

    public EmailRequestBuilder addresses(final List<EmailAddress> addresses) {
        this.addresses = addresses;
        return this;
    }

    public EmailRequestBuilder subject(String subject) {
        this.subject = subject;
        return this;
    }

    public EmailRequestBuilder content(String content) {
        this.content = content;
        return this;
    }

    public EmailRequestBuilder addAddress(final String address) {
        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        this.addresses.add(new EmailAddress(address));
        return this;
    }

    public EmailRequest build() {
        return new EmailRequest(addresses, subject, content);
    }
}
