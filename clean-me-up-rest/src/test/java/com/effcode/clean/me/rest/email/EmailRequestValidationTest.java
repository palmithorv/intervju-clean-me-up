package com.effcode.clean.me.rest.email;

import com.effcode.clean.me.rest.ValidationTest;
import org.junit.Test;

import javax.validation.ConstraintViolation;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.emptyList;


/**
 * @author palmithor
 * @since 2018-08-30
 */
public class EmailRequestValidationTest extends ValidationTest {


    @Test
    public void allValid() {
        assertThat(validator.validate(EmailDataUtils.requestAllValid().build())).hasSize(0);
    }

    @Test
    public void failAddressesIsNull() {
        final ConstraintViolation<EmailRequest> violation = validateAndGetSingleViolation(EmailDataUtils.requestAllValid()
                .addresses(null)
                .build());
        assertListMustNotBeEmpty(violation, "addresses", true);
    }

    @Test
    public void failAddressesIsEmpty() {
        final ConstraintViolation<EmailRequest> violation = validateAndGetSingleViolation(EmailDataUtils.requestAllValid()
                .addresses(emptyList())
                .build());
        assertListMustNotBeEmpty(violation, "addresses", false);
    }

    @Test
    public void failAddressIsInvalid() {
        final ConstraintViolation<EmailRequest> violation = validateAndGetSingleViolation(EmailDataUtils.requestAllValid()
                .addAddress("notemail")
                .build());
        assertThat(violation.getMessage()).isEqualTo("must be a well-formed email address");
    }

    @Test
    public void failSubjectIsNull() {
        final ConstraintViolation<EmailRequest> violation = validateAndGetSingleViolation(EmailDataUtils.requestAllValid()
                .subject(null)
                .build());
        assertNotBlank(violation, "subject", true);
    }

    @Test
    public void failSubjectIsBlank() {
        final ConstraintViolation<EmailRequest> violation = validateAndGetSingleViolation(EmailDataUtils.requestAllValid()
                .subject("")
                .build());
        assertNotBlank(violation, "subject", false);
    }

    @Test
    public void failContentExceedsSize() {
        final String invalidValue = IntStream.range(0, 650001).boxed().map(Objects::toString).collect(Collectors.joining());
        final ConstraintViolation<EmailRequest> violation = validateAndGetSingleViolation(EmailDataUtils.requestAllValid()
                .content(invalidValue)
                .build());
        assertSize(violation, "content", invalidValue, 0, 65000);
    }
}