package com.effcode.clean.me.rest.email;

import com.effcode.clean.me.support.SmtpHandler;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

/**
 * @author palmithor
 * @since 2018-08-30
 */
@RunWith(SpringRunner.class)
public class EmailServiceTest {

    @MockBean private SmtpHandler handler;

    private EmailService emailService;

    @Before
    public void setUp() {
        emailService = new EmailService(handler, "username", "password");
    }

    @Test
    public void send() {
        doNothing().when(handler).post(any());
        assertThatCode(() -> emailService.send(EmailDataUtils.requestAllValid().build()))
                .doesNotThrowAnyException();
    }

    @Test
    public void sendFailed() {
        doThrow(new RuntimeException("Error")).when(handler).post(any());
        assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> emailService.send(EmailDataUtils.requestAllValid().build()))
                .withMessage("Error");
    }
}